var IMAGE = {
	TILE_ZXY: '../images/tiles/{z}/{x}/{y}.png',
	TILE_ERROR: '../images/tiles/5/0/0.png',
	PIN_START: '../images/pin_start.png',
	PIN_END: '../images/pin_end.png',
	PIN_EVENT: '../images/eventPin.png',
	PIN_RESTAURANT: '../images/resPin.png'
};