var MapObject = function(coord, map, rc, layer) {
	this.coord = coord;
	this.map = map;
	this.rc = rc;
	this.layer = layer;
};

MapObject.prototype.show = function() {
	if(this.showOverride)
		this.showOverride();
	else
		this.map.addLayer(this.layer);
	return this;
};

MapObject.prototype.hide = function() {
	if(this.hideOverride)
		this.hideOverride();
	else
		this.map.removeLayer(this.layer);
	return this;
};

MapObject.prototype.getCoord = function() {
	return this.coord;
};

MapObject.prototype.getLatLng = function() {
	return this.layer.getLatLng();
};

MapObject.prototype.move = function(newPos) {
	this.coord = newPos;
	this.layer.setLatLng(this.rc.unproject(newPos));
	return this;
};

var MapObject_proto = function (){};
MapObject_proto.prototype = MapObject.prototype;