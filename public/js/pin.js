var Pin = function(imgUrl, coord, map, rc, iconSize, iconAnchor) {
	MapObject.call(this, coord, map, rc,
		L.marker(rc.unproject(coord), {
			icon: L.icon({
				iconUrl: imgUrl,
				iconSize: iconSize,
				iconAnchor: iconAnchor
			})
		}));

	var self = this;

	this.layer.on('dragend', function (event) {
		self.dragend();
	});
};

Pin.prototype = new MapObject_proto();

Pin.prototype.dragend = function () {
	var project = this.rc.project(this.getLatLng());
	this.coord = [Math.round(project['x']), Math.round(project['y'])];

	self.map.findRouteBetweenPins();
};

var Pin_proto = function () {};
Pin_proto.prototype = Pin.prototype;