var Map = function(minZoom, maxZoom, img) {
	this.layer = L.map('map', {
		minZoom: minZoom,
		maxZoom: maxZoom,
		attributionControl: false,
		zoomControl: false
	});

	if(screen.width > 1000)
		this.addLayer(
			L.control.zoom({
				position: 'topleft'
		}));

	this.rc = new L.RasterCoords(this.layer, img);
	this.rc.setMaxBounds();

	this.setView(this.rc.unproject([5700, 1650]), 3);

	this.addLayer(
		L.tileLayer(IMAGE.TILE_ZXY, {
			noWrap: true,
			errorTileUrl: IMAGE.TILE_ERROR
	}));

	this.searchPin = new SearchPin([1000, 1000], this, this.rc);
	this.startPin = new Pin(
		IMAGE.PIN_START,
		[1000, 1000],
		this,
		this.rc,
		[53.25, 57],
		[26.25, 57]
	);
	this.endPin = new Pin(
		IMAGE.PIN_END,
		[3000, 1500],
		this,
		this.rc,
		[53.25, 57],
		[26.25, 57]
	);
	this.gpsPin = L.marker([0, 0], {
		color: 'red'
	});

	this.removeLayer(this.startPin);
	this.removeLayer(this.endPin);
	this.layer.removeLayer(this.gpsPin);

	this.resList = {
		"학생회관식당학생회관(63동)":[5478,2173],
		"3식당전망대(75-1동)":[6000,2477],
		"기숙사식당관악사(919동)":[4686,976],
		"자하연식당농협(109동)":[5176,1882],
		"302동식당302동":[7451,1862],
		"솔밭간이식당110동":[5852,1490],
		"동원관식당113동":[4354,2014],
		"감골식당101동":[4730,2248],
		"4식당서당골 (76동)":[5223,1406],
		"두레미담75-1동":[6100,2477],
		"301동식당301동":[7201,1871],
		"예술계식당(74동)74동":[4996,1796],
		"샤반501동":[5332,2487],
		"공대간이식당30-2동":[5871,2146],
		"소담마루동원생활관 113동 3층":[4404,2014],
		"220동식당3단계대학원연구동(220동)":[4523,1650],
		"라운지오동원생활관(113동) 1층":[4454,2014]
	}
	this.eventPin = new Pin(
		IMAGE.PIN_EVENT,
		[1500,1500],
		this,
		this.rc,
		[53.25, 57],
		[26.25, 57]
	);
	this.resPins = {};
	// for(var i=0;i<this.restaurants.length;i++){
	// 	this.eventPins[i] = new Pin('../images/resPin.png', [this.resLat[i],this.resLong[i]], this, this.rc, [53.25, 57], [26.25, 57]);
	// }
	for(key in this.resList){
		this.resPins[key] = new Pin(
			IMAGE.PIN_RESTAURANT,
			this.resList[key],
			this,
			this.rc,
			[53.25, 57],
			[26.25, 57]
		);
	}

	var self = this;

	this.gps = false;
	$('#gps_button').removeClass('green');
	$('#gps_button').addClass('blue');
	$('#gps_button').on('click', function () {
		self.toggleGPS();
	})

	this.eventPins = [];
	this.eventMode = 0;
	this.resMode = 0;
	this.weddingMode = 0;
};

Map.prototype.addLayer = function (target) {
	target.addTo(this.layer);
};

Map.prototype.removeLayer = function (target) {
	this.layer.removeLayer(target);
};

Map.prototype.setView = function (latlng, zoomLevel) {
	this.layer.setView(latlng, zoomLevel);
};

Map.prototype.fitBounds = function (bounds) {
	this.layer.fitBounds(bounds);
};

var facilities_on = function() {
	L.addTo(Map.getMap());
};

Map.prototype.drawPolyline = function(data) {
	$('#navipopup').show();
	if(!this.busRoute) {
		this.busRoute = [];
	}
	else {
		for (var key in this.busRoute) {
			var polyline = this.busRoute[key];
			polyline.hide();
		}

		this.busRoute.splice(0, this.busRoute.length);
	}
	if(this.walkRoute) {
		this.walkRoute.hide();
	}

	this.walkRoute = new WalkRoute(data.normal, this, this.rc);
	$('#navipopup_walk').html('도보 - 약 ' + data.normalDist + '분');

	if('bus' in data) {
		$('#navipopup_bus').show();
		$('#navipopup_bus').addClass('active');
		$('#navipopup_walk').removeClass('active');
		$('#navipopup_bus').html('셔틀버스 - 약 ' + data.busDist + '분');
		for(var key in data.bus) {
			var sub = data.bus[key];
			if(sub.level == 1) {
				this.busRoute.push((new WalkRoute(sub, this, this.rc)).show());
			}
			else {
				this.busRoute.push((new BusRoute(sub, this, this.rc)).show());
			}
		}
	}
	else {
		$('#navipopup_bus').hide();
		$('#navipopup_walk').addClass('active');
		$('#navipopup_bus').removeClass('active');
		$('#navipopup_walk').show();
		this.walkRoute.show();
	}

	var bounds = L.featureGroup([this.startPin.layer, this.endPin.layer]).getBounds();
	for (var key in this.busRoute) {
		var polyline = this.busRoute[key];
		polyline.show();
		bounds = bounds.extend(polyline.getBounds());
	}
	bounds.extend(this.walkRoute.layer.getBounds());
	this.fitBounds(bounds);
};

Map.prototype.showWalkRoute = function () {
	$('#navipopup_walk').addClass('active');
	$('#navipopup_bus').removeClass('active');
	this.walkRoute.show();
	for (var key in this.busRoute) {
		var polyline = this.busRoute[key];
		polyline.hide();
	}
};

Map.prototype.showBusRoute = function () {
	$('#navipopup_bus').addClass('active');
	$('#navipopup_walk').removeClass('active');
	this.walkRoute.hide();
	for (var key in this.busRoute) {
		var polyline = this.busRoute[key];
		polyline.show();
	}
}

Map.prototype.findRouteBetweenNodesByIdx = function() {
	var self = this;
	if(self.startIdx && self.endIdx) {
		$.get(
			'/navi/byIndices',
			{from : self.startIdx, to : self.endIdx},
			function (data) {
				var parsed = JSON.parse(data);
				self.drawPolyline(parsed);
			}
		);
		$('.after_searching_menu').fadeIn();
	}
	else if(self.startIdx) {
		self.setView(self.startPin.getLatLng(), 4);
	}
	else {
		self.setView(self.endPin.getLatLng(), 4);
	}
};

Map.prototype.findRouteBetweenCoords = function(startCoord, endCoord) {
	var self = this;

	$.get(
		'/navi/byCoords',
		{fromLat : startCoord[0], fromLng : startCoord[1],
			toLat : endCoord[0], toLng : endCoord[1]},
		function(data) {
			self.drawPolyline(JSON.parse(data));
		}
	);
};

Map.prototype.findRouteBetweenPins = function() {
	var startCoord = this.startPin.getCoord();
	var endCoord = this.endPin.getCoord();
	this.findRouteBetweenCoords([startCoord[0], startCoord[1]], [endCoord[0], endCoord[1]]);
};

Map.prototype.findByIdx = function (idx) {
	if (!idx)
		return;

	var self = this;
	// console.log(this);
	console.log(idx);

	$.get(
		'/getBuildingInfo',
		{idx: idx},
		function (data) {
			self.searchPin.show();
			self.searchPin.move([data.latitude, data.longitude]);
			self.searchPin.info = data;
			self.setView(self.searchPin.getLatLng(), 4);
			__INFO__.setSearchedData(data);
		});
		$('.after_searching_menu').fadeIn();
};

Map.prototype.setStartIdx = function (idx) {
	var self = this;

	$.get(
		'/getBuildingInfo',
		{idx: idx},
		function (data) {
			self.startIdx = idx;
			self.startPin.move([data.latitude, data.longitude]);
			self.startPin.show();
			self.findRouteBetweenNodesByIdx();
			__INFO__.setSearchedData(data);
		});
};

Map.prototype.setEndIdx = function (idx) {
	var self = this;

	$.get(
		'/getBuildingInfo',
		{idx: idx},
		function (data) {
			self.endIdx = idx;
			self.endPin.move([data.latitude, data.longitude]);
			self.endPin.show();
			self.findRouteBetweenNodesByIdx();
			__INFO__.setSearchedData(data);
		});
};

Map.prototype.findByValue = function (idx) {
	if (!idx)
		return;

	var self = this;

	$.get(
		'/getAnySearchInfo',
		{idx: idx},
		function (data) {
			__INFO__.getArriveSearchbar().map.clear();
			self.searchPin.show();
			self.searchPin.move([data.latitude, data.longitude]);
			self.searchPin.info = data;
			self.setView(self.searchPin.getLatLng(), 4);
			self.searchPin.layer.bindPopup("<b>" + data.name + "</b><br>" + data.note).openPopup();
			$('#modal_menu_plus').closeModal();
			$('.button-collapse').sideNav('hide');
			if (__INFO__.getState())
				__INFO__.changeState()
			__INFO__.getSearchbar().selectize[0].selectize.setValue(__INFO__.findIdxByNumber(data.number));
		});
};

Map.prototype.setState = function (state) {
	// false: search, true: navi
	this.clear();

	if(state == true) {
		delete this.startIdx;
		delete this.endIdx;
	}
};

Map.prototype.clear = function () {
	this.searchPin.hide();
	this.startPin.hide();
	this.endPin.hide();
	if(this.polyline)
		this.removeLayer(this.polyline);
	for(var key in this.busRoute) {
		this.busRoute[key].hide();
	}
};

Map.prototype.setEvent = function() {
	this.layer.on('click', this.eventOnClick);
}

Map.prototype.offEvent = function() {
	this.layer.off('click', this.eventOnClick);
	this.eventPin.hide();
}

Map.prototype.eventOnClick = function(e) {
	var coord = this.rc.project([e.latlng['lat'],e.latlng['lng']]);
	console.log(coord);
	$("#event_latitude").val(Math.floor(coord.x));
	$("#event_longitude").val(Math.floor(coord.y));
	this.eventPin.move([coord.x,coord.y]);
	this.eventPin.show();
	$('.eventToast').html("<a class='waves-effect waves-light btn' style='margin-left:0'><div class='modal-trigger' id='eventTestButton' href='#modal_menu'>장소 확정</div></a>");
	$('#eventTestButton').leanModal();
}

Map.prototype.showEvent = function() {
	var self = this;
	var now = new Date().toISOString().substring(0,10);
	console.log(now);
	$.get(
	      '/get_event',
	      {date: now},
	      function (data){
	      	console.log(data);
	      	if(data.length > 0){
	      	for(var i=0;i<data.length;i++){
	      		self.eventPins[i] = new Pin(IMAGE.PIN_EVENT, [data[i]['latitude'], data[i]['longitude']], self, self.rc, [53.25, 57], [26.25, 57]);
	      		self.eventPins[i].layer.bindPopup("<div style='color:red'>" + data[i]['event_name'] + "</div>" + '주최:' + data[i]['host_name'] + '<br>' + '날짜:' + data[i]['event_date'] + '<div>' + data[i]['event_text'] + '</div>');
	      		self.eventPins[i].show();
	      		}
	      	}
	      	else {
	      		alert("오늘은 행사가 없습니다");
	      	}
	      });
}

Map.prototype.showMenu = function() {
	var self = this;
	var rc = this.rc;
	var pins = this.resPins;
	var pinOption = {
		"MaxWidth": 10
	}
	var res = this.restaurants;
	var menus = [];
	$.get(
		'/crawling',
		function (data) {
			var i = -1;
			for (var key in data) {
				if(data[key].length == 0 || !(key in pins)){
					continue;
				}
				else {
					var menuStr = "<div style='font-size:10px'>".concat(key);
					var timeMenu = ["<div style='color:red'>아침</div>","<div style='color:red'>점심</div>","<div style='color:red'>저녁</div>"];
					var timeIndex = 3 - data[key].length;
					for(var k=0;k<data[key].length;k++){
						if(data[key].length == 1){
							menuStr = menuStr.concat(timeMenu[1]);
						}
						else{
							menuStr = menuStr.concat(timeMenu[k+timeIndex]);
						}
						for(menuName in data[key][k]){
							if(data[key][k][menuName] != "1000"){
								menuStr = menuStr.concat("<div style='color:blue'>₩");
								menuStr = menuStr.concat(data[key][k][menuName]);
								menuStr = menuStr.concat("</div>");
							}
							var menuArr = menuName.split(",");
							if(menuArr.length > 2){
								for(var i=0;i<menuArr.length;i++){
									menuStr = menuStr.concat(menuArr[i]);
									if(i<menuArr.length - 1)
										menuStr = menuStr.concat(",");
									if(i%2 == 1)
										menuStr = menuStr.concat("<br>");
								}
							}
							else{
								menuStr = menuStr.concat(menuName);
							}
						}
					}
					menuStr = menuStr.concat("</div>");
					self.addLayer(pins[key].layer.bindPopup(menuStr));
				}
			}
		});
};

Map.prototype.toggleWedding = function () {
	if(!this.weddingPin) {
		this.weddingPinShow = true;
		this.weddingPin = {};

		for(var key in this.spotDic) {
			var spotInfo = this.spotDic[key];
			if(spotInfo.class == 'wedding') {
				this.weddingPin[key] = new SpotPin(this.spotDic[key], this, this.rc);
				this.weddingPin[key].show();
			}
		}
	}
	else {
		if(this.weddingPinShow == false) {
			this.weddingPinShow = true;
			for(var key in this.weddingPin) {
				this.weddingPin[key].show();
			}
		}
		else {
			this.weddingPinShow = false;
			for(var key in this.weddingPin) {
				this.weddingPin[key].hide();
			}
		}
	}
}

Map.prototype.toggleGPS = function () {
	if ("geolocation" in navigator) {
		if(this.gps) {
			this.gps = false;
			$('#gps_button').removeClass('green');
			$('#gps_button').addClass('blue');
			this.removeLayer(this.gpsPin);
			if(this.watchID) {
				navigator.geolocation.clearWatch(this.watchID);
			}
		}
		else {
			this.gps = true;
			$('#gps_button').removeClass('blue');
			$('#gps_button').addClass('green');
			this.addLayer(this.gpsPin);

			var self = this;
			navigator.geolocation.getCurrentPosition(
				function (position) {
					var lat = position.coords.latitude;
					var lng = position.coords.longitude;
					var x = 7389605 - 188511 * lat - 2541.45 * lng;
					var y = 19131010 - 672.83 * lat - 150481 * lng;
					if(x < 0 || x > 8192 || y < 0 || y > 8192) {
						alert("학내를 벗어난 위치입니다.");
						self.gps = false;
						$('#gps_button').removeClass('green');
						$('#gps_button').addClass('blue');
						self.removeLayer(self.gpsPin);
					}
					else {
						self.gpsPin.setLatLng(self.rc.unproject([x, y]));
						self.fitBounds(L.featureGroup([self.gpsPin]).getBounds());
					}
				},
				function () {
					alert("GPS 정보를 가져올 수 없습니다.");
					self.gps = false;
					$('#gps_button').removeClass('green');
					$('#gps_button').addClass('blue');
					self.removeLayer(self.gpsPin);
				},
				{enableHighAccuracy: true}
			);

			this.watchID = navigator.geolocation.watchPosition(
				function (position) {
					var lat = position.coords.latitude;
					var lng = position.coords.longitude;
					var x = 7389605 - 188511 * lat - 2541.45 * lng;
					var y = 19131010 - 672.83 * lat - 150481 * lng;
					if(x < 0 || x > 8192 || y < 0 || y > 8192) {
						alert("학내를 벗어난 위치입니다.");
						self.gps = false;
						$('#gps_button').removeClass('green');
						$('#gps_button').addClass('blue');
						self.removeLayer(self.gpsPin);
					}
					else {
						self.gpsPin.setLatLng(self.rc.unproject([x, y]));
					}
				},
				function () {
					alert("GPS 정보를 가져올 수 없습니다.");
					self.gps = false;
					$('#gps_button').removeClass('green');
					$('#gps_button').addClass('blue');
					self.removeLayer(self.gpsPin);
					navigator.geolocaion.clearWatch(this.watchID);
				},
				{enableHighAccuracy: true}
			);
		}
	}
	else {
		alert("GPS 정보를 가져올 수 없습니다.");
	}
};