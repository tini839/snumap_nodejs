var authorization = function () {
	var account = document.getElementById('mysnu_id').value;
	console.log("account: " + account);
	$.post(
		'/login_modules/require_authmail',
		{ account: account },
		function (data) {
			if(data.success) {
				location.href = '/authorization_mail';
			}
			else {
				if(data.error == "already authorized") {
					alert("이미 구성원 인증이 완료된 SNU 계정입니다.\n이 계정으로는 구성원 인증을 받을 수 없습니다.");
				}
				else
					alert('내부 서버 오류입니다.');
			}
		}
	);
}

$(document).ready(function(){
	if($('#map')) {
		var map = new Map(0, 5, [8192, 8192]);
		window.map = map;
		if('sideNav' in $('.button-collapse')){
			$('.button-collapse').sideNav();
		}
		$.get(
			'/getAllBuildingInfo',
			function (data) {
				var searchbar = new SearchBarBase('#searchbar', map, data, 'findByIdx');
				var departSearchbar = new SearchBarBase('#departbar', map, data, 'setStartIdx');
				var arriveSearchbar = new SearchBarBase('#arrivebar', map, data, 'setEndIdx');
				__INFO__.setState(false);
				__INFO__.setData(data);
				__INFO__.setSearchbar(searchbar, departSearchbar, arriveSearchbar);
			});

		$.get(
			'/getAnySearch',
			function (data) {
				var searchbar = new SearchBarBase('#anysearch', map, data, 'findByValue');
			});

		$.get(
			'/getAllBusstopInfo',
			function (data) {
				map.busstopDic = data;
			});

		$.get(
			'/getAllSpotInfo',
			function (data) {
				map.spotDic = data;
			});

		$('#navipopup').hide();

		$('#navipopup_walk').click(function () {
			if ($('#navipopup_walk').hasClass('active')) {
				return;
			}
			else {
				map.showWalkRoute();
			}
		});

		$('#navipopup_bus').click(function () {
			if ($('#navipopup_bus').hasClass('active')) {
				return;
			}
			else {
				map.showBusRoute();
			}
		});
	}
});