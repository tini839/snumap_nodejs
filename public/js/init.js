// internal properties
function Info()
{
	var state = false; // search: false, road: true
	var login_id = undefined;
	var data = undefined;
	var searchedData = undefined;
	var searchbar = undefined;
	var departSearchbar = undefined;
	var arriveSearchbar = undefined;
	var searchIdx = undefined;
	var departIdx = undefined;
	var arriveIdx = undefined;

	this.setData = function(_data) {
		data = _data;
	};

	this.getData = function (_data) {
		return data;
	};

	this.setSearchIdx = function (idx) {
		searchIdx = idx;
	}

	this.setRouteIdx = function (d_idx, a_idx) {
		departIdx = d_idx;
		arriveIdx = a_idx;
	}

	this.setSearchbar = function (s, ds, as) {
		searchbar = s;
		departSearchbar = ds;
		arriveSearchbar = as;
		if (searchIdx)
			s.selectize[0].selectize.setValue(searchIdx);
		else if (departIdx)
		{
			this.changeState();
			ds.selectize[0].selectize.setValue(departIdx);
			as.selectize[0].selectize.setValue(arriveIdx);
		}
	};

	this.getSearchbar = function () {
		return searchbar;
	}

	this.getDepartSearchbar = function () {
		return departSearchbar;
	};

	this.getArriveSearchbar = function() {
		return arriveSearchbar;
	};

	this.changeState = function()
	{
		data = [];
		if (state == true)
		{
			$('#search_icon').text('directions');
			$('#navbar_search').show();
			$('#navbar_navi').hide();
			//$('#nav_form').html('<div style="padding-top: 12px;" class="container_form"><form><div class="selectize input-field"><select id="searchbar" placeholder="검색" class="selectbar"></select></div></form></div>');
			//searchbar = new SearchBarBase('#searchbar', map, data, 'findByIdx');
		} else {
			$('#search_icon').text('search');
			$('#navbar_search').hide("fast",function(){
				$('#navbar_navi').show("fast");
			});
			//$('#nav_form').html('<div style="padding-top: 12px;" class="container_form"><form><div class="selectize input-field"><select id="depart" placeholder="출발" class="selectbar"></select></div></form></div><div style="padding-top: -12px;" class="container_form"><form><div class="selectize input-field"><select id="arrive" placeholder="도착" class="selectbar"></select></div></form></div>');
			//depart = new SearchBarBase('#depart', map, data, 'findByIdx');
			//arrive = new SearchBarBase('#arrive', map, data, 'findByIdx');
		}
		state = !state;
		window.map.setState(state);
	};

	this.setState = function (newState) {
		state = !newState;
		this.changeState();
	}

	this.isLogined = function () {
		if (login_id == undefined)
			return false;
		return true;
	}

	this.getState = function () {
		return state;
	}

	this.setId = function() {
		$.post('/login_modules/getId', {}, function(data, status) {
			if (data != "")
				login_id = data;
			console.log(data);
		});
	}

	this.getId = function() {
		return login_id;
	}

	this.setSearchedData = function(data) {
		searchedData = data;
	}

	this.getSearchedData = function() {
		return searchedData;
	}

	this.findIdxByNumber = function(n) {
		console.log(n);
		console.log(data[n].number);
		for (var i = 0; i < Object.keys(data).length; i++) {
			if (data[i] != undefined)
			{
				if (data[i].number == n)
					return i;
			}
		}
	}
}

// Access internal variables with INFO
var __INFO__ = new Info();
__INFO__.setId();

// $(function() {
// 	$('#filter_first').click(function() {
// 		$('.filter_child').css('color', 'gray');
// 		$(this).css('color', 'black');
// 	});
// 	$('#filter_second').click(function() {
// 		$('.filter_child').css('color', 'gray');
// 		$(this).css('color', 'black');
// 	});
// 	$('#filter_third').click(function() {
// 		$('.filter_child').css('color', 'gray');
// 		$(this).css('color', 'black');
// 	});
// 	$('#filter_fourth').click(function() {
// 		$('.filter_child').css('color', 'gray');
// 		$(this).css('color', 'black');
// 	});
// });
