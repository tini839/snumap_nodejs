var SearchPin = function(coord, map, rc) {
	MapObject.call(this, coord, map, rc, L.marker(rc.unproject(coord)));
};

SearchPin.prototype = new MapObject_proto();