var WalkRoute = function(data, map, rc) {
	var linePath = [];

	if(data.startCoord)
		linePath.push(rc.unproject(data.startCoord));

	for (var i=0; i<data.subroute.length; i++){
		linePath.push(rc.unproject(data.subroute[i].coord));
	}

	if(data.endCoord)
		linePath.push(rc.unproject(data.endCoord));

	MapObject.call(this, [], map, rc, L.polyline(linePath, {color: 'blue'}));
};

WalkRoute.prototype = new MapObject_proto();

WalkRoute.prototype.getBounds = function () {
	return this.layer.getBounds();
};