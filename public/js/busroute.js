var BusRoute = function(data, map, rc) {
	var linePath = [];

	this.busline = data.busline;
	var popup = '';

	for (var key in this.busline) {
		var name = this.busline[key];

		if(name == 'circle')
			name = '순환셔틀';
		else if(name == 'circle_rev')
			name = '역방향셔틀';
		popup += name;
	}

	for (var i=0; i<data.subroute.length; i++){
		linePath.push(rc.unproject(data.subroute[i].coord));
	}

	this.map = map;

	this.getonPopup = L.popup({closeButton: false}).setLatLng(linePath[0]).setContent(
			map.busstopDic[data.subroute[0].idx] + ' 정류장<br />' + popup + '승차');
	this.getoffPopup = L.popup({closeButton: false}).setLatLng(linePath[linePath.length - 1]).setContent(
			map.busstopDic[data.subroute[data.subroute.length - 1].idx] + ' 정류장<br />' + popup + '하차')
	this.busMarker = L.marker(linePath[0], {icon: L.icon({iconUrl: '../images/bus.png', iconSize: [30, 30], iconAnchor: [15, 15]})});
	this.tapped = false;

	var self = this;

	this.busMarker.on('click', function (e) {
		if(self.tapped) {
			map.removeLayer(self.getonPopup);
			map.removeLayer(self.getoffPopup);
			self.tapped = false;
		}
		else {
			map.addLayer(self.getonPopup);
			map.addLayer(self.getoffPopup);
			self.tapped = true;
		}
	});
	MapObject.call(this, [], map, rc, L.polyline(linePath, {color: 'red'}));
};

BusRoute.prototype = new MapObject_proto();

BusRoute.prototype.getBounds = function () {
	return L.featureGroup([this.layer, this.busMarker, this.getonPopup, this.getoffPopup]).getBounds();
};

BusRoute.prototype.showOverride = function () {
	this.tapped = false;
	this.map.addLayer(this.layer);
	this.map.addLayer(this.busMarker);
	this.map.removeLayer(this.getonPopup);
	this.map.removeLayer(this.getoffPopup);
};

BusRoute.prototype.hideOverride = function () {
	this.tapped = false;
	this.map.removeLayer(this.layer);
	this.map.removeLayer(this.busMarker);
	this.map.removeLayer(this.getonPopup);
	this.map.removeLayer(this.getoffPopup);
};