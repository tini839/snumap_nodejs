var MapObject = function (coord, map, rc, layer) {
	this.coord = coord;
	this.map = map;
	this.rc = rc;
	this.layer = layer;
};

MapObject.prototype.show = function() {
	this.map.map.addLayer(this.layer);
};

MapObject.prototype.hide = function() {
	this.map.map.removeLayer(this.layer);
};

MapObject.prototype.getCoord = function() {
	return this.coord;
};

MapObject.prototype.getLatLng = function() {
	return this.layer.getLatLng();
};

MapObject.prototype.move = function(newPos) {
	this.coord = newPos;
	this.layer.setLatLng(newPos);
};

var MapObject_proto = function (){};
MapObject_proto.prototype = MapObject.prototype;

var MapTool = function (map) {
	var self = this;

	self.nodePinDic = {};
	self.map = map;
	self.newNodeIdx = 0;
	self.modifyList = [];

	self.map.map.on('click', function (event) {
		if(window.mode == 'move') {
			var project = self.map.rc.project(event.latlng);
			console.log(project);
		}
		else if(window.mode == 'node1') {
			var project = self.map.rc.project(event.latlng);
			var coord = [Math.round(project['x']), Math.round(project['y'])];
			self.addNodePin(coord, 1);
		}
		else if(window.mode == 'node2') {
			var project = self.map.rc.project(event.latlng);
			var coord = [Math.round(project['x']), Math.round(project['y'])];
			self.addNodePin(coord, 2);
		}
	});
};

MapTool.prototype.load = function () {
	var self = this;

	$.get(
		'/maptool/load',
		function (data) {
			self.modifyList.splice(0, self.modifyList.length);
			self.clearElements();
			var nodes = JSON.parse(data);

			$.each(nodes, function (key, node) {
				var nodePin = new NodePin(node.idx, node.coord, self.map, self.map.rc, node.level, node.bus);
				self.nodePinDic[node.idx] = nodePin;
			});

			$.each(nodes, function (key, node) {
				var nodePin = self.nodePinDic[node.idx];
				$.each(node.neighbors, function (k, neighIdx) {
					if(Number(neighIdx) in self.nodePinDic) {
						var neighborNodePin = self.nodePinDic[Number(neighIdx)];
						if(node.idx in neighborNodePin.edges)
							nodePin.addEdge(neighIdx, neighborNodePin.edges[node.idx]);
						else
							nodePin.addEdge(neighIdx, new EdgeLine(nodePin, neighborNodePin, self.map, self.map.rc));
					}
				});
			});
			self.drawElements(0);
		});
};

MapTool.prototype.save = function () {
	var self = this;

	var datas = [];

	$.each(self.modifyList, function (k, idx) {
		var nodePin = self.nodePinDic[idx];
		if(nodePin) {
			var edges = [];
			$.each(nodePin.edges, function (key, val) {
				edges.push(key);
			});
			datas.push({
				idx: idx,
				exist: true,
				coord: nodePin.coord,
				neighbors: edges,
				level: nodePin.level
			});
		}
		else {
			datas.push({
				idx: idx,
				exist: false
			});
		}
	});

	$.post(
		'/maptool/save',
		{data: datas},
		function (data, textStatus, jqXHR) {
			if(data.success === true) {
				console.log('success');
				self.modifyList.splice(0, self.modifyList.length);
			}
			else
				console.log('failed');
		},
		'json'
	);
};

MapTool.prototype.drawElements = function (level) {
	var self = this;

	$.each(self.nodePinDic, function (key, val) {
		if(level != 0 && val.level != level)
			return;
		val.show();
		$.each(val.edges, function (k, edge) {
			edge.show();
		});
	});
};

MapTool.prototype.hideElements = function () {
	var self = this;

	$.each(self.nodePinDic, function (key, val) {
		val.hide();
		$.each(val.edges, function (k, edge) {
			edge.hide();
		});
	});
};

MapTool.prototype.redraw = function (level) {
	this.hideElements();
	this.drawElements(level);
}

MapTool.prototype.clearElements = function () {
	var self = this;

	self.hideElements();
	$.each(self.nodePinDic, function (key, val) {
		delete self.nodePinDic[key];
	});
};

MapTool.prototype.addToModifyList = function (idx) {
	if($.inArray(idx, this.modifyList) < 0) {
		this.modifyList.push(idx);
	}
};

MapTool.prototype.addNodePin = function (coord, level) {
	while(this.newNodeIdx in this.nodePinDic) {
		this.newNodeIdx++;
	}

	var nodePin = new NodePin(this.newNodeIdx, coord, this.map, this.map.rc, level, null);
	this.nodePinDic[Number(this.newNodeIdx)] = nodePin;
	this.addToModifyList(nodePin.idx);
	nodePin.show();
};

MapTool.prototype.removeNodePin = function (idx) {
	delete this.nodePinDic[idx];
};

MapTool.prototype.addEdgeLine = function (idx1, idx2) {
	var node1 = this.nodePinDic[idx1];
	var node2 = this.nodePinDic[idx2];
	var edge = new EdgeLine(node1, node2, this.map, this.map.rc);
	this.nodePinDic[idx1].edges[idx2] = edge;
	this.nodePinDic[idx2].edges[idx1] = edge;
	this.addToModifyList(idx1);
	this.addToModifyList(idx2);
	edge.show();
};

var coverage = function (coord, radius, map, rc) {
	var coords = [];
	for(i = 0; i < 25; i++) {
		var cos = Math.cos(2 * Math.PI * i / 25);
		var sin = Math.sin(2 * Math.PI * i / 25);
		coords.push(rc.unproject([coord[0] + radius * cos, coord[1] + radius * sin]));
	}
	return L.polygon(coords).addTo(map);
}

var NodePin = function (idx, coord, map, rc, level, bus) {
	MapObject.call(this, coord, map, rc,
		L.marker(rc.unproject(coord), {
				icon: L.icon({
					iconUrl: 'images/maptool/' + (Number(level) == 2? 'busnode.png' : 'node.png'),
					iconSize: [10, 10],
					iconAnchor: [5, 5],
					zIndexOffset: 2,
				}),
				draggable: true,
			}));

	this.idx = idx;
	this.edges = {};
	this.level = Number(level);

	var self = this;

	if(bus && bus['circle']) {
		this.coverage = new coverage(coord, 350, map.map, rc);
	}

	this.layer.on('click', function (event) {
		console.log(self.idx);
		if(window.mode == 'edge') {
			if(window.tmpNode == 0) {
				window.tmpNode = self.idx;
			}
			else {
				window.mapTool.addEdgeLine(window.tmpNode, self.idx);
				window.tmpNode = 0;
			}
		}
	});

	this.layer.on('drag', function (event) {
		self.drag();
	});

	this.layer.on('contextmenu', function (event) {
		self.destroy();
	});
};


NodePin.prototype = new MapObject_proto();

NodePin.prototype.addEdge = function (other, edgeLine) {
	this.edges[other] = edgeLine;
};

NodePin.prototype.removeEdge = function (other) {
	delete this.edges[other];
};

NodePin.prototype.drag = function () {
	window.mapTool.addToModifyList(this.idx);
	var project = this.rc.project(this.getLatLng());
	this.coord = [Math.round(project['x']), Math.round(project['y'])];
	$.each(this.edges, function (key, edge) {
		edge.redraw();
	});
};

NodePin.prototype.destroy = function () {
	var self = this;

	window.mapTool.addToModifyList(self.idx);
	$.each(this.edges, function (key, edge) {
		delete self.edges[key];
		window.mapTool.addToModifyList(key);
		window.mapTool.nodePinDic[key].removeEdge(self.idx);
	});
	window.mapTool.removeNodePin(this.idx);
	this.hide();
};

NodePin.prototype.show = function () {
	this.map.map.addLayer(this.layer);
};

var EdgeLine = function (node1, node2, mapClass, rc) {
	var self = this;
	self.directed = false;
	if(node1.level == 2 && node2.level == 2)
		MapObject.call(this, [], mapClass, rc, L.polyline([[0, 0], [0, 0]], {color: 'red'}));
	else
		MapObject.call(this, [], mapClass, rc, L.polyline([[0, 0], [0, 0]], {color: 'blue'}));
	self.node1 = node1;
	self.node2 = node2;
	self.redraw();

	this.layer.on('contextmenu', function (event) {
		self.destroy();
	});
};

EdgeLine.prototype = new MapObject_proto();

EdgeLine.prototype.redraw = function () {
	var coord1 = this.rc.unproject(this.node1.coord);
	var coord2 = this.rc.unproject(this.node2.coord);
	this.layer.setLatLngs([coord1, coord2]);
};

EdgeLine.prototype.destroy = function () {
	window.mapTool.addToModifyList(this.node1.idx);
	window.mapTool.addToModifyList(this.node2.idx);
	this.node1.removeEdge(this.node2.idx);
	this.node2.removeEdge(this.node1.idx);
	this.hide();
};

EdgeLine.prototype.show = function () {
	this.map.map.addLayer(this.layer);
};

$(document).ready(function() {
	var mapTool = new MapTool(window.map);
	window.mapTool = mapTool;
	window.mode = 'move';
	window.tmpNode = 0;

	$('#load-button').click(function () {mapTool.load();});
	$('#save-button').click(function () {mapTool.save();});
	$('#move-button').click(function () {
		$('#move-button').addClass('active');
		$('#node1-button').removeClass('active');
		$('#edge-button').removeClass('active');
		$('#node2-button').removeClass('active');
		window.mode = 'move';
	});
	$('#node1-button').click(function () {
		$('#move-button').removeClass('active');
		$('#node1-button').addClass('active');
		$('#edge-button').removeClass('active');
		$('#node2-button').removeClass('active');
		window.mode = 'node1';
	});
	$('#edge-button').click(function () {
		$('#move-button').removeClass('active');
		$('#node1-button').removeClass('active');
		$('#edge-button').addClass('active');
		$('#node2-button').removeClass('active');
		window.mode = 'edge';
		window.tmpNode = 0;
	});
	$('#node2-button').click(function () {
		$('#move-button').removeClass('active');
		$('#node1-button').removeClass('active');
		$('#edge-button').removeClass('active');
		$('#node2-button').addClass('active');
		window.mode = 'node2';
	});

	$('#all-button').click(function () {
		$('#all-button').addClass('active');
		$('#level1-button').removeClass('active');
		$('#level2-button').removeClass('active');
		mapTool.redraw(0);
	});

	$('#level1-button').click(function () {
		$('#all-button').removeClass('active');
		$('#level1-button').addClass('active');
		$('#level2-button').removeClass('active');
		mapTool.redraw(1);
	});

	$('#level2-button').click(function () {
		$('#all-button').removeClass('active');
		$('#level1-button').removeClass('active');
		$('#level2-button').addClass('active');
		mapTool.redraw(2);
	});
});