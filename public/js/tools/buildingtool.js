var Map = function (minZoom, maxZoom, img, callback) {
	var self = this;

	this.map = L.map('map', {
		minZoom: minZoom,
		maxZoom: maxZoom,
		attributionControl: false,
		zoomControl: false,
	});

	this.img = img;
	this.pinDic = {};
	this.modifyList = [];
	this.newPinIdx = 0;

	if(screen.width > 1000)
		L.control.zoom({position: 'topleft'}).addTo(this.map);

	this.rc = new L.RasterCoords(this.map, this.img);
	this.rc.setMaxBounds();

	this.map.setView(this.rc.unproject([img[0]/2, img[1]/2]), 3);

	L.tileLayer('../images/tiles/{z}/{x}/{y}.png', {
		noWrap: true,
	}).addTo(this.map);

	this.map.on('click', function (event) {
		var project = self.rc.project(event.latlng);
		var coord = [Math.round(project['x']), Math.round(project['y'])];
		self.addPin(coord);
	})

	callback();
};

Map.prototype.addToModifyList = function (idx) {
	if($.inArray(idx, this.modifyList) < 0) {
		this.modifyList.push(idx);
	}
};

Map.prototype.load = function (data) {
	var self = this;

	$.each(self.pinDic, function (key, val) {
		val.destroy();
	})

	$.each(data, function (key, val) {
		self.pinDic[Number(val.idx)] = new Pin(val.idx, val.number, val.name, val.name_eng, [val.latitude, val.longitude], self, self.rc);
	});

	self.modifyList.splice(0, self.modifyList.length);
};

Map.prototype.save = function () {
	var self = this;

	var datas = [];

	$.each(self.modifyList, function (k, idx) {
		var pin = self.pinDic[idx];
		if(pin) {
			datas.push({
				idx: idx,
				exist: true,
				coord: pin.coord,
				name: pin.name,
				name_eng: pin.name_eng,
				number: pin.number
			});
		}
		else {
			datas.push({
				idx: idx,
				exist: false
			});
		}
	});

	console.log(datas);

	$.post(
		'/buildingtool/save',
		{data: datas},
		function (data, textStatus, jqXHR) {
			if(data.success === true) {
				console.log('success');
				self.modifyList.splice(0, self.modifyList.length);
			}
			else
				console.log('failed');
		},
		'json'
	);
};

Map.prototype.addPin = function (coord) {
	while(this.newPinIdx in this.pinDic) {
		this.newPinIdx++;
	}

	var pin = new Pin(this.newPinIdx, 'NONE', 'NONE', 'NONE', coord, this, this.rc);
	this.pinDic[this.newPinIdx] = pin;
	this.addToModifyList(pin.idx);
}

var Pin = function (idx, number, name, name_eng, coord, map, rc) {
	this.idx = idx;
	this.number = number;
	this.name = name;
	this.name_eng = name_eng;
	this.coord = coord;
	this.map = map;
	this.rc = rc;
	this.layer = L.marker(rc.unproject(coord), {draggable: true});

	var self = this;

	self.popupcontent = document.createElement('a');
	self.popupcontent['data-value'] = this.idx;
	self.updatePopup();
	self.popupcontent.onclick = function() {
		var pr = prompt('건물 이름 입력', '');
		if(pr)
	    	self.name = pr;
	    pr = prompt('영어 이름 입력', '');
	    if(pr)
	    	self.name_eng = pr;
	    pr = prompt('동번호 입력', '');
	    if(pr)
	    	self.number = pr;
	    self.updatePopup();
	    self.map.addToModifyList(self.idx);
	};

	this.layer.bindPopup(self.popupcontent).openPopup();

	this.layer.on('drag', function (event) {
		self.drag();
	});

	this.layer.on('contextmenu', function (event) {
		self.destroy();
	});

	this.show();
};

Pin.prototype.drag = function () {
	this.map.addToModifyList(this.idx);
	var project = this.rc.project(this.layer.getLatLng());
	this.coord = [Math.round(project['x']), Math.round(project['y'])];
	this.popupcontent.innerHTML = this.name + '<br />' + this.name_eng + '<br />' + this.number + '<br />' + this.coord;
};

Pin.prototype.destroy = function () {
	this.map.addToModifyList(this.idx);
	delete this.map.pinDic[this.idx];
	this.hide();
};

Pin.prototype.show = function() {
	this.map.map.addLayer(this.layer);
};

Pin.prototype.hide = function() {
	this.map.map.removeLayer(this.layer);
};

Pin.prototype.updatePopup = function () {
	this.popupcontent.innerHTML = this.name + '<br />' + this.name_eng + '<br />' + this.number + '<br />' + this.coord;
}


/*
 * leaflet plugin for plain image map projection
 * @copyright 2015 commenthol
 * @license MIT
 */

/* globals L */
/*
 * L.RasterCoords
 * @param {L.map} map - the map used
 * @param {Array} imgsize - [ width, height ] image dimensions
 * @param {Number} [tilesize] - tilesize in pixels. Default=256
 */
L.RasterCoords = function (map, imgsize, tilesize) {
	this.map = map;
	this.width = imgsize[0];
	this.height = imgsize[1];
	this.tilesize = tilesize || 256;
	this.zoom = this.zoomLevel();
};

L.RasterCoords.prototype = {
	/*
	 * calculate accurate zoom level for the given image size
	 */
	zoomLevel: function() {
		return Math.ceil(
					Math.log(
						Math.max(this.width, this.height)/
						this.tilesize
					)/Math.log(2)
				);
	},
	/*
	 * unproject `coords` to the raster coordinates used by the raster image projection
	 * @param {Array} coords - [ x, y ]
	 * @return {L.LatLng} - internal coordinates
	 */
	unproject: function(coords) {
		return this.map.unproject(coords, this.zoom);
	},
	/*
	 * project `coords` back to image coordinates
	 * @param {Array} coords - [ x, y ]
	 * @return {L.LatLng} - image coordinates
	 */
	project: function(coords) {
		return this.map.project(coords, this.zoom);
	},
	/*
	 * sets the max bounds on map
	 */
	setMaxBounds: function() {
		var southWest = this.unproject([0, this.height]);
		var northEast = this.unproject([this.width, 0]);
		this.map.setMaxBounds(new L.LatLngBounds(southWest, northEast));
	}
};

$(document).ready(function(){
	var map = new Map(0, 5, [8192, 7280], function () {
		window.map = map;
		if('sideNav' in $('.button-collapse')){
			$('.button-collapse').sideNav();
		}
		$('#load-button').click(function () {
			$.get(
				'/getAllBuildingInfo',
				function (data) {
					map.load(data);
			});
		});
		$('#save-button').click(function () {map.save();});


	});
})
