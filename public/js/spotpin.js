var SpotPin = function (data, map, rc) {
	var coord = [Number(data.latitude), Number(data.longitude)];
	var unproject = rc.unproject(coord);
	this.popup = L.popup().setLatLng(unproject).setContent(data.name);
	map.removeLayer(this.popup);
	this.clicked = false;

	var self = this;

	if(data.class == 'wedding')
		Pin.call(this, '../images/pin_wedding.png', coord, map, rc, [44, 38], [22, 38]);

	this.layer.on('click', function (event) {
		if(self.clicked) {
			self.clicked = false;
			map.removeLayer(self.popup);
		}
		else {
			self.clicked = true;
			map.addLayer(self.popup);
		}
	});
};

SpotPin.prototype = new Pin_proto();