var SearchBarBase = function(id, map, data, onChangeCallback) {
	var self = this;
	self.map = map;
	var options = [];

	$.each(data, function (key, val) {
		options.push({
			name: val.name != 'NONE'? val.name : '',
			info: val.number != 'NONE'? (val.number + '동') : '',
			idx: val.idx
		});
	});
	self.selectize = $(id).selectize({
		maxItems: 1,
		valueField: 'idx',
		labelField: 'name',
		searchField: ['name', 'info'],
		sortField: 'name',
		options: options,
		render: {
	        item: function(item, escape) {
	            return '<div>' +
	                (item.name ? '<span class="name">' + escape(item.name) + '</span>' : '') +
	                (item.info ? '<span class="info">, ' + escape(item.info) + '</span>' : '') +
	            '</div>';
	        },
	        option: function(item, escape) {
	            var label = item.name || item.info;
	            var caption = item.name ? item.info : null;
	            return '<div>' +
	                '<span class="label">' + escape(label) + '</span>' +
	                (caption ? '<span class="caption">' + escape(caption) + '</span>' : '') +
	            '</div>';
	        }
	    },
	    create: false,
	    onChange: function (value) {
	    	self.map[onChangeCallback](value);
	    }
	});
};

SearchBarBase.getMap = function() {
	return this.map;
};