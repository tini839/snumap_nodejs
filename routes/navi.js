'use strict';

var express = require('express');
var router = express.Router();
var fs = require('fs');
var Node = require('../snumap_modules/node')
var NodeTable = require('../snumap_modules/nodeTable')
var BuildingTable = require('../snumap_modules/buildingTable')
var Navigator = require('../snumap_modules/navigator')

router.get('/', function(req, res, next) {
  res.render('navi');
});

router.get('/byIndices', function(req, res, next) {
	res.format({
		'text/plain': function(){
			let startNode = BuildingTable.getBuildingInfo(Number(req.query.from))
			let endNode = BuildingTable.getBuildingInfo(Number(req.query.to))
			let startCoord = [Number(startNode.latitude), Number(startNode.longitude)]
			let endCoord = [Number(endNode.latitude), Number(endNode.longitude)]
			let normal = (new Navigator(startCoord, endCoord, 1, 1)).getRoute()
			let bus = (new Navigator(startCoord, endCoord, 2, 1)).getRoute()

			if(Math.abs(normal.distance - bus.distance) < 5) {
				res.send({normal: normal,
					normalDist: Math.round(normal.distance / 100)
				})
			}
			else {
				res.send({
					normal: normal
					, bus: divideRoute(bus)
					, normalDist: Math.round(normal.distance / 100)
					, busDist: Math.round(bus.distance / 100)
				})
			}
		}
	})
})

router.get('/byCoords', function(req, res, next) {
	res.format({
		'text/plain': function(){
			let startCoord = [Number(req.query.fromLat), Number(req.query.fromLng)]
			let endCoord = [Number(req.query.toLat), Number(req.query.toLng)]
			let normal = (new Navigator(startCoord, endCoord, 1, 1)).getRoute()
			let bus = (new Navigator(startCoord, endCoord, 2, 1)).getRoute()

			if(Math.abs(normal.distance - bus.distance) < 5) {
				res.send({normal: normal,
					normalDist: Math.round(normal.distance / 100)
				})
			}
			else {
				res.send({
					normal: normal
					, bus: divideRoute(bus)
					, normalDist: Math.round(normal.distance / 100)
					, busDist: Math.round(bus.distance / 100)
				})
			}
		}
	})
})

var divideRoute = function (busRoute) {
	try {
	var result = [];

	var curLevel = 0;
	var sublist = [];
	var busline = {};
	for (var i = 0; i < busRoute.subroute.length; i++) {
		var node = busRoute.subroute[i];
		if(curLevel == 0)
			curLevel = node.level;

		if(curLevel == 1 && node.level == 1) {
			sublist.push(node);
		}
		else if(curLevel == 2 && node.level == 2) {
			var pass = 0;
			for(var key in busline) {
				if(busline[key] == node.idx)
					pass++;
			}
			if(node.bus && pass > 0) {
				var count = 0;
				for(var key in busline) {
					if(busline[key] == node.idx && node.bus[key]) {
						busline[key] = node.bus[key].next;
						count++;
					}
				}
				sublist.push(node);
				if(count <= 0) {
					var line = [];
					for(var key in busline) {
						if(node.bus[key] && busline[key] == node.bus[key].next)
							line.push(key);
					}
					result.push({level: 2, subroute: sublist, busline: line});
					for(var key in node.bus)
						busline[key] = node.bus[key].next;
					sublist = [node];
				}
			}
			else {
				sublist.push(node);
			}
		}
		else if (curLevel == 1 && node.level == 2) {
			sublist.push(node);
			result.push({level: 1, subroute: sublist});
			curLevel = 2;
			sublist = [node];
			for(var key in node.bus)
				busline[key] = node.bus[key].next;
		}
		else {
			var line = [];
			for(var key in busline) {
				var last = sublist[sublist.length - 1];
				if(key in last.bus && busline[key] == last.bus[key].next)
					line.push(key);
			}
			result.push({level: 2, subroute: sublist, busline: line});
			curLevel = 1;
			sublist = [sublist[sublist.length - 1]];
			sublist.push(node);
		}
	}

	result.push({level: 1, subroute: sublist, endCoord: busRoute.endCoord});
	result[0]['startCoord'] = busRoute.startCoord;

	for (var key in result) {
		var sub = result[key];
		if (!sub.startCoord && !sub.endCoord && sub.subroute.length <= 1)
			result.splice(key, 1);
	}
} catch (err) {console.log(err.stack);}
	return result;
}

function intersect_safe(a, b)
{
  var ai=0, bi=0;
  var result = new Array();

  while( ai < a.length && bi < b.length )
  {
     if      (a[ai] < b[bi] ){ ai++; }
     else if (a[ai] > b[bi] ){ bi++; }
     else /* they're equal */
     {
       result.push(a[ai]);
       ai++;
       bi++;
     }
  }

  return result;
}

module.exports = router;
