var express = require('express');
var router = express.Router();
var DBConnection = require('../snumap_modules/DBConnection');
var nodemailer = require('nodemailer');
var smtpTransport = require('nodemailer-smtp-transport');
var SHA256 = require('crypto-js/sha256');
module.exports = router;

var smtpTransport = nodemailer.createTransport(smtpTransport({
	service: 'Gmail',
	auth: {
		user: 'chloe@krazi.net',
		pass: '159453shfl!'
	}
}));

var sendAuthMail = function (user, address, callback) {
	var encrypted = SHA256(address).toString();
		var mailOptions = {
			from: 'chloe@krazi.net',
			to: address,
			subject: '스누맵 인증 메일',
			html: '<p>다음 링크를 클릭하면 인증이 완료됩니다.</p><p><a href=\"http://snumap.com/login_modules/require_auth?authkey=' + encrypted + '\">인증하기</a></p>'
		};

		var query = "UPDATE user SET authkey = '" + encrypted + "' WHERE email = '" + user + "';";
		DBConnection.query(query, function (err, rows, fields) {
			if (err)
				throw err;
			else {
				smtpTransport.sendMail (mailOptions, function (error, response) {
					callback(error);
					smtpTransport.close();
				});
			}
		});
}

var checkSNUAuth = function (address, trueCallback, falseCallback) {
	var encrypted = SHA256(address).toString();
	var query = "SELECT * FROM user WHERE authkey = '" + encrypted + "' AND auth = 1;";
	var result = false;
	DBConnection.query(query, function (err, rows, fields) {
		if (err) {
			throw err;
			return;
		}
		else if(rows.length > 0)
			trueCallback();
		else
			falseCallback();
	});
}

/* login */
router.post ('/submit', function (req, res) {
  console.log ("login called");
  console.log ("email: " + req.body.email);
  console.log ("passwd: " + req.body.passwd);

  var query = "SELECT * FROM user WHERE email='" + req.body.email.toString () + "';";
  DBConnection.query (query, function (err, rows, fields) {
    if (err) throw err;
    if (rows[0]) {
      if (rows[0].passwd == req.body.passwd) {
        // res.send (true);
        req.session.user = req.body.email.toString();
        req.session.name = rows[0].first_name + ' ' + rows[0].last_name;
        req.session.fav = rows[0].favorite ? rows[0].favorite.split(',') : new Array();
        req.session.auth = Number(rows[0].auth);
				console.log (rows[0]);
        console.log ("email: " + req.session.user);
        console.log ("name: " + req.session.name);
        console.log ("fav: " + req.session.fav);
        console.log ("auth: " + req.session.auth);
        // return res.render ('index', {login: true, id: req.body.email});
/*
        if(Number(rows[0].auth) == 0) {
        	// not authenticated account
        	res.send({success: false, error: 'not authenticated'});
        	return;
        }*/
        res.send ({success: true});
        return;
      }
    }
    res.send ({success: false});
  });
});

/* new account */
router.post ('/new_account',
		function (req, res, next) {
			var query = "SELECT passwd FROM user WHERE email = '" + req.body.email.toString () + "';";
			DBConnection.query (query, function (err, rows, fields) {
				if (err) throw err;
				if (rows[0]) {
					console.log ("account exists");
					res.send ({success: false, error: "account exists"});
					return;
				}
				next ();
			});
		},
		function (req, res, next) {
			var first_name = req.body.first_name;
			var last_name = req.body.last_name;
			var email = req.body.email;
			var passwd = req.body.passwd;
			var query = "INSERT INTO user (email, passwd, first_name, last_name) VALUES ('" + email + "', '" + passwd + "', '"+ first_name + "', '" + last_name + "');";
			DBConnection.query (query, function (err, result) {
				if (err) {
					throw err;
					return;
				}

				//snumail check
				var length = email.length;
				if(email.substring(length - 10, length) == "@snu.ac.kr") {
					checkSNUAuth(email,
						function () {
							res.send({success: true, snumail: false, error: "already authorized"})
						},
						function () {
							sendAuthMail(email, email, function (error) {
								if(error) {
									throw error;
									res.send({success: true, snumail: false, error: "server problem"});
								}
								else {
									res.send({success: true, snumail: true});
								}
							});
						});
				}
				else {
					res.send ({success: true, snumail: false, error: "not snumail"});
				}
			});
		}
);

router.post('/getId',
	function (req, res, next) {
		res.send (req.session.user);
	}
);

router.post('/confirm_password',
		function (req, res, next) {
			var query = "SELECT passwd FROM user WHERE email = '" + req.session.user + "';";
			DBConnection.query (query, function (err, rows, fields) {
				if (err) throw err;
				if (rows[0]) {
					if (req.body.passwd == rows[0].passwd) {
						res.send (true);
						return;
					}
					res.send (false);
				}
			});
		}
);

router.post ('/change_account',
	function (req, res, next) {
		var first_name = req.body.first_name;
		var last_name = req.body.last_name;
		var passwd = req.body.passwd;
		var query = "UPDATE user SET first_name='" + first_name + "', last_name='" + last_name + "', passwd='" + passwd + "' WHERE email='" + req.session.user + "';";
		DBConnection.query (query, function (err, result) {
			if (err) throw err;
				res.send (true);
		});
	}
);

router.post ('/logout',
	function (req, res) {
		if (req.session.user) {
			req.session.destroy ();
			res.clearCookie('sid');
			// console.log ("req.session:\n" + req.session.user + ", " + req.session.name);
		}
		res.send (true);
	}
);

router.post ('/require_authmail', function (req, res, next) {
	if (req.session.user) {
		var address = req.body.account + "@snu.ac.kr";
		checkSNUAuth(address,
			function () {
				res.send({success: false, error: "already authorized"});
			},
			function () {
				sendAuthMail(req.session.user, address, function (error) {
					if (error) {
						res.send({success: false, error: "server problem"});
					}
					else {
						res.send({success: true});
					}
				});
			});
	}
});



router.get ('/require_auth', function (req, res, next) {
	if(req.param('authkey')) {
		var query = "UPDATE user SET auth = 1 WHERE authkey = '" + req.param('authkey') + "';";

		DBConnection.query(query, function (err, rows, fields) {
			if (err)
				throw err;
			else {
				req.session.auth = 1;
				res.render('authorization_complete');
			}
		})
	}
});

router.post ('/withdraw', function (req, res, next) {
	if (req.session.user) {
		var query = "DELETE FROM user WHERE email = '" + req.body.email + "' AND passwd = '" + req.body.passwd + "';";
		query += "DELETE FROM event WHERE host_name = '" + req.body.email + "';"

		DBConnection.query(query, function (err, rows, fields) {
			if (err) {
				res.send({success: false});
			}

			else {
				req.session.destroy ();
				res.clearCookie('sid');
				res.send({success: true});
				return;
			}
		})
	}
	else {
		res.send({success: false});
	}
});
