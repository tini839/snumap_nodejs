var express = require('express');
var router = express.Router();
var DBConnection = require('../snumap_modules/DBConnection');

/* mysql connection */
router.post('/', function(req,res){
  req.body['promotion_start'] = req.body['promotion_start'] + " 00:00:00";
  req.body['promotion_end'] = req.body['promotion_end'] + " 00:00:00";
  req.body['user'] = req.session.user;
  var query = DBConnection.query('insert into event set ?',req.body,function(err,result){
    if(err){
      throw err;
      console.log(err);
      res.render('add_fail');
    }
    else{
      console.log('event added');
      res.render('add_success');
    }
  })
})

module.exports = router;
