var express = require('express');
var router = express.Router();
var BuildingTable = require('../snumap_modules/buildingTable');
var BusstopTable = require('../snumap_modules/busstopTable');
var SpotTable = require('../snumap_modules/spotTable');
var AnySearch = require('../snumap_modules/anySearch');
var DBConnection = require('../snumap_modules/DBConnection');
var Navigator = require('../snumap_modules/navigator');

/* GET home page. */
router.get('/', function(req, res, next) {
	if (req.session.user) {   // 세션에 user 가 존재하면 (인증됨)
		console.log ("logined");
		console.log ("user session email: " + req.session.user);
		res.render('index', { login: true, name: req.session.name, email: req.session.user, auth: req.session.auth, s: req.param('s') ? req.param('s') : 'undefined', ds: req.param('ds') ? req.param('ds') : 'undefined', as: req.param('as') ? req.param('as') : 'undefined'});
		return;
	}
	console.log ("not logined");
	res.render('index', {login: false, s: req.param('s') ? req.param('s') : 'undefined', ds: req.param('ds') ? req.param('ds') : 'undefined', as: req.param('as') ? req.param('as') : 'undefined'});

});

router.get('/getAllBuildingInfo', function(req, res, next) {
	res.send(BuildingTable.dic);
});

router.get('/getBuildingInfo', function(req, res, next) {
	res.send(BuildingTable.getBuildingInfo(Number(req.query.idx)));
});

router.get('/getAnySearchInfo', function(req, res, next) {
	console.log (AnySearch.getAnySearchInfo(Number(req.query.idx)));
	res.send(AnySearch.getAnySearchInfo(Number(req.query.idx)));
});

router.get('/getAnySearch', function(req, res, next) {
	res.send(AnySearch.dic);
});

/* user modules */
router.get('/change_account_info', function(req, res, next) {
	var name = req.session.name.split(' ');
	res.render('change_account_info', { first_name: name[0], last_name: name[1] });
});

/* favorite */
router.post('/favorite', function(req, res, next) {
	res.send(req.session.fav);
});

router.post('/addFav',
	function(req, res, next) {
		console.log ("id: " + req.body.favId);
		if (req.session.fav.indexOf(req.body.favId) != -1) {
			res.send(false);
			return;
		}
		var sub = '';
		for (var i = 0; i < req.session.fav.length; i++) {
			sub += req.session.fav[i] + ',';
		}
		req.session.fav.push(req.body.favId);
		sub += req.body.favId;
		var query = 'UPDATE user SET favorite="' + sub + '" WHERE email="' + req.session.user + '";';
		console.log(query);
		DBConnection.query (query, function (err, result) {
			if (err) throw err;
			res.send(true);
		});
		console.log("success");
	}
);

router.post('/removeFav',
	function(req, res, next) {
		console.log(req.body.rmId);
		req.session.fav.splice(req.session.fav.indexOf(req.body.rmId), 1);
		var sub = '';
		for (var i = 0; i < req.session.fav.length - 1; i++) {
			sub += req.session.fav[i] + ',';
		}
		sub += req.session.fav[req.session.fav.length-1];
		var query = 'UPDATE user SET favorite="' + sub + '" WHERE email="' + req.session.user + '";';
		console.log(query);
		DBConnection.query (query, function (err, result) {
			if (err) throw err;
			res.send(true);
		});
		console.log("success");
	}
);

/* login */
router.get('/login', function(req, res, next) {
  res.render('login');
});

/* new account */
router.get('/new_account', function(req, res, next) {
  res.render('new_account');
});

/* change account info */
router.get('/change_account_info', function(req, res, next) {
  res.render('change_account_info');
});

/* confirm password */
router.get('/confirm_account', function(req, res, next) {
  res.render('confirm_account');
});

/* authorization */
router.get('/authorization', function(req, res, next) {
  res.render('authorization');
});

/* authorization_mail */
router.get('/authorization_mail', function(req, res, next) {
  res.render('authorization_mail');
});

/* withdrawal */
router.get('/withdrawal', function(req, res, next) {
  res.render('withdrawal', {email: req.session.user});
});

router.get('/getAllBusstopInfo', function(req, res, next) {
	res.send(BusstopTable.dic);
});

router.get('/getAllSpotInfo', function(req, res, next) {
	res.send(SpotTable.dic);
});

router.get('/cmap/navi/navigator.php', function(req, res, next) {
	var startNode = BuildingTable.getBuildingInfo(Number(req.query.from))
	var endNode = BuildingTable.getBuildingInfo(Number(req.query.to))
	var startCoord = [Number(startNode.latitude), Number(startNode.longitude)]
	var endCoord = [Number(endNode.latitude), Number(endNode.longitude)]
	var normal = (new Navigator(startCoord, endCoord, 1, 1)).getRoute()

	var coords = [startCoord];
	for(var i = 0; i < normal.subroute.length; i++) {
		coords.push(normal.subroute[i].coord);
	}
	coords.push(endCoord);

	res.send(coords);
});

module.exports = router;
