'use strict';

var express = require('express');
var router = express.Router();
var Node = require('../snumap_modules/node')
var nodeTable = require('../snumap_modules/nodeTable');
var buildingTable = require('../snumap_modules/buildingTable');

router.get('/', function(req, res, next) {
	res.render('buildingtool');
});

router.post('/save', function(req, res, next) {
	buildingTable.modify(req.body.data, function () {
		res.format({
			'text/plain': function() {
				res.send({success: true});
			}
		});
	});
});

module.exports = router;