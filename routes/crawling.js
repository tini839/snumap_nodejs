'use strict';
// Url crawling API using npm crawler 0.4.3
// made by Tony Kim
// 스누 맵 화이팅입니다. 비록 나이가 들어 서울대 지리를 대충 다 알고 있지만
// 스누 맵으로 인해 많은 신입생들이 더 즐거운 학교 생활을 보낼 수 있을 것 같습니다

var express = require('express');
var router = express.Router();
var Crawler = require('crawler');
var url = require('url');
var apicache = require('apicache').options({ debug: true }).middleware;
// price mapping
// var price_mapping = {
// 	'ⓐ': 2500,
// 	'ⓑ': 3000,
// 	'ⓒ': 3500,
// 	'ⓓ': 4000,
// 	'ⓔ': 4500,
// 	'ⓕ': 5000,
// 	'ⓖ': 'etc',
// 	'ⓙ': 6000
// }
var price_mapping2 = {
	'ⓐ': 1700,
	'ⓑ': 2000,
	'ⓒ': 2500,
	'ⓓ': 3000,
	'ⓔ': 3500,
	'ⓕ': 4000,
	'ⓖ': 4500,
	'ⓗ': 5000,
	'ⓕ': 4000,
	'ⓙ': 5500,
	'ⓘ': 6000
};

var special_mapping = {
	'한식':1000,
	'한일':1000,
	'피자':1000};

var string_processing = function(raw_string){
	var string = raw_string.replace(/\s\s+/g,'  ');
	var splitted_string = string.split('  ');
	var result_dict = {};
	var cache_key = splitted_string[0];
	var temp_dict = {};
	var temp_list = [];
	splitted_string.forEach(function(string){
		if (string.length == 0){
			return;
		}
		if (string.substring(0,1) in price_mapping2) {
			if (string.substring(3,4) == '5') return;
			var menu_list = string.replace(/\s+/g, '/').split('/');
			menu_list.forEach(function(menu){
				var price = price_mapping2[menu.substring(0,1)];
				var name = menu.substring(1);
				temp_dict[name] = price;
			});
			temp_list.push(temp_dict);
			temp_dict = {};
		} else{
			result_dict[cache_key] = temp_list;
			temp_list = [];
			cache_key = string;
		}
	});
	return result_dict
}

var string_processing2 = function(raw_string, result, callback){
	var string = raw_string.replace(/\s\s+/g,'  ');
	var splitted_string = string.split('  ');
	var result_dict = {};
	var cache_key = splitted_string[0];
	var temp_dict = {};
	var temp_list = [];
	splitted_string.forEach(function(string){
		if (string.length == 0){
			result_dict[cache_key] = temp_list;
			temp_list = [];
			cache_key = string;
			return;
		}
		else if (string.substring(0,2) in special_mapping){
			temp_dict[string] = 1000;
			temp_list.push(temp_dict);
			temp_dict = {};
		}
		else if (string.substring(0,1) in price_mapping2) {
			if (string.substring(3,4) == '5') return;
			var menu_list = string.replace(/\s+/g, '/').split('/');
			menu_list.forEach(function(menu){
				if (menu == "7000")
					return;
				if (menu == "치킨탕수육"){
					temp_dict["치킨탕수육"] = 7000;
					return;
				}
				var price = price_mapping2[menu.substring(0,1)];
				var name = menu.substring(1);
				temp_dict[name] = price;
			});
			temp_list.push(temp_dict);
			temp_dict = {};
		} else{
			result_dict[cache_key] = temp_list;
			temp_list = [];
			cache_key = string;
		}
	});
	return result = result_dict;
}

var mergeDic = function extend(obj, src) {
  for (var key in src) {
      if (src.hasOwnProperty(key)) obj[key] = src[key];
  }
  return obj;
}

router.get('/', apicache('30 minutes'), function(req, res, next) {
	// Crawling initialize
	var c = new Crawler({
    maxConnections : 10,
    forceUTF8 : true,
    incomingEncoding : 'euc-kr',
    // This will be called for each crawled page
    callback : function (error, result, $) {
    }
	});

	var result_dictionary;

	var a = c.queue([{
	    uri: 'http://www.snuco.com/html/restaurant/restaurant_menu1.asp',
	 	jQuery: {
	 		name: 'cheerio',
	 		options: {
	 			normalizeWhitespace: true
	 		}
	 	},
	    // The global callback won't be called
	    callback: function (error, result, $) {
	    	if(error) {
	    		console.log('parsing error!');
	    		res.json('');
	    	}
				result_dictionary = $('table').text().substring(150);
				c.queue([{
			 	 	  uri: 'http://www.snuco.com/html/restaurant/restaurant_menu2.asp',
				 		jQuery: {
				 		name: 'cheerio',
				 		options: {
				 			normalizeWhitespace: true
				 			}
				 		},
				    // The global callback won't be called
				    callback: function (error, result, $) {
				    	if(error) {
				    		console.log('parsing error!');
				    		res.json('');
				    	}
							var result2;
							res.json(mergeDic(string_processing(result_dictionary),string_processing2($('table').text().substring(150), result2, function () {
							res.json(mergeDic(result_dictionary, result2));
							})));
    				}
				}]);
    	}
	}]);


});

module.exports = router
