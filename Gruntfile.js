module.exports = function(grunt) {

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		uglify: {
			options: {
				banner: '/* <%= grunt.template.today("yyyy-mm-dd") %> */',
				mangle: true,
				preserveComments: false
			},
			build: {
				src: 'public/build/snumap.js',
				dest: 'public/build/snumap.min.js'
			}
		},

		concat: {
			basic: {
				src: [
				'public/js/init.js',
				'public/js/mapobject.js',
				'public/js/map.js',
				'public/js/searchpin.js',
				'public/js/pin.js',
				'public/js/spotpin.js',
				'public/js/walkroute.js',
				'public/js/busroute.js',
				'public/js/rastercoords.js',
				'public/js/searchbarbase.js',
				'public/js/index.js',
				'public/js/const.js'
				],
				dest: 'public/build/snumap.js'
			}
		},

		removelogging: {
			dist: {
				src: 'public/build/snumap.js'
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-remove-logging');

	grunt.registerTask('default', ['concat', 'removelogging', 'uglify']);
};