'use strict'

var DBConnection = require('./DBConnection');

var AnyInfo = function (idx, name, number, note, latitude, longitude) {
	this.idx = Number(idx);
	this.name = name;
	this.number = number;
	this.note = note;
	this.latitude = Number (latitude);
	this.longitude = Number (longitude);
};

var AnySearch = function () {
	this.dic = {};
	this.load();
};

AnySearch.prototype.load = function (path) {
	var self = this;

	var query = "select name, number, note, latitude, longitude from anysearch_s union select name, number, note, latitude, longitude from anysearch_p";
	DBConnection.query (query, function (error, rows, fields) {
		if (error)
			throw error;
		else {
			var idx = 0;
			rows.forEach(function (row, idx, arr) {
				self.dic[Number(idx)] = new AnyInfo(idx++, row.name, row.number, row.note, row.latitude, row.longitude);
			});
		}
	});
};

AnySearch.prototype.getAnySearchInfo = function (idx) {
	return this.dic[idx];
}

module.exports = new AnySearch();
