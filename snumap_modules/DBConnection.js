var mysql = require('mysql');

var connection = mysql.createConnection({
	host: "localhost",
	user: "smap",
	port: 3306,
	password: "1qa2ws3ed!",
	database: "snumap",
	multipleStatements: true
});

connection.connect (function (error) {
	if (error) {
		console.log ("Problem with MySQL: " + error);
		throw error;
	}
	else {
		console.log ("Database connection success");
	}
});

module.exports = connection;
