'use strict'

var DBConnection = require('./DBConnection');

var getChoSung = function(name) {
	var cho = ["ㄱ","ㄲ","ㄴ","ㄷ","ㄸ","ㄹ","ㅁ","ㅂ","ㅃ","ㅅ","ㅆ","ㅇ","ㅈ","ㅉ","ㅊ","ㅋ","ㅌ","ㅍ","ㅎ"];
  var result = "";
  for(var i=0;i<name.length;i++) {
    var code = name.charCodeAt(i)-44032;
    if(code>-1 && code<11172) result += cho[Math.floor(code/588)];
  }
  return result;
}

var getAbbrCommon = function(name) {
	var abbrList = [];
	var index = 0;

	var first = name.charAt(0);
	var last = name.charAt(name.length - 1);

	// 첫글자와 끝글자 조합
	abbrList[index] = first.concat(last);
	index++;

	// 첫글자와 중간글자 조합
	var middle = name.charAt(name.length/2);
	abbrList[index] = first.concat(middle);
	index++;

	// 첫글자와 세번째 글자
	var third = name.charAt(2);
	abbrList[index] = first.concat(third);
	index++;

	return abbrList;
}

var getAbbrAll = function(name) {
	var abbrList = [];
	var index = 0;

	var first = name.charAt(0);
	var last = name.charAt(name.length - 1);

	for(var i=0;i<name.length;i++){
		for(var j=i+1;j<name.length;j++){
			for(var k=j+1;k<name.length;k++){
				var temp = name.charAt(i).concat(name.charAt(j),name.charAt(k));
				abbrList[index] = temp;
				index++;
			}
			var temp = name.charAt(i).concat(name.charAt(j));
			abbrList[index] = temp;
			index++;
		}
	}

	return abbrList;
}

var BuildingInfo = function (idx, number, name, name_eng, latitude, longitude) {
	this.idx = Number(idx);
	this.number = number;
	this.name = name;
	this.name_eng = name_eng;
	this.latitude = Number(latitude);
	this.longitude = Number(longitude);
	this.chosung = getChoSung(name);
	this.abbrAll = getAbbrAll(name);
	this.abbrCommon = getAbbrCommon(name);
};

var BuildingTable = function () {
	this.dic = {};
	this.load();
};

BuildingTable.prototype.load = function (path) {
	var self = this;

	var query = "SELECT * FROM snumap";
	DBConnection.query (query, function (error, rows, fields) {
		if (error)
			throw error;
		else {
			rows.forEach(function (row, idx, arr) {
				self.dic[Number(row.idx)] = new BuildingInfo(row.idx, row.number, row.name, row.name_eng, row.latitude, row.longitude);
			});
		}
	});
};

BuildingTable.prototype.setBuildingInfo = function (idx, number, name, name_eng, latitude, longitude) {
	this.dic[Number(idx)] = new BuildingInfo (idx, number, name, name_eng, latitude, longitude);
};

BuildingTable.prototype.getBuildingInfo = function (idx) {
	return this.dic[idx];
};

BuildingTable.prototype.modify = function (data, callback) {
	var query = '';

	if (!data) {
		callback();
		return;
	}

	for (var key in data) {
		var node = data[key];
		var idx = Number(node.idx);

		if(node.exist == 'false') {
			delete this.dic[idx];
			query += 'DELETE FROM snumap WHERE idx = ' + idx + ';\n';
		}
		else {
			var latitude = Number(node.coord[0]);
			var longitude = Number(node.coord[1]);

			var info = new BuildingInfo (
				idx,
				node.number? node.number : '',
				node.name? node.name : '',
				node.name_eng? node.name_eng : '',
				latitude,
				longitude
				);

			this.dic[idx] = info;
			query += 'INSERT INTO snumap (idx, number, name, name_eng, latitude, longitude)\n';
			query += 'VALUES (' + info.idx + ', \'' + info.number + '\', \'' + info.name + '\', \'' + info.name_eng + '\', ' + info.latitude + ', ' + info.longitude + ')\n';
			query += 'ON DUPLICATE KEY\n';
			query += 'UPDATE number = \'' + info.number + '\', name = \'' + info.name + '\', name_eng = \'' + info.name_eng + '\', latitude = ' + info.latitude + ', longitude = ' + info.longitude + ';\n';
		}
	}
	console.log(query);

	DBConnection.query (query, function (error, rows, fields) {
		if (error)
			throw error;
		else
			callback();
	});
};

module.exports = new BuildingTable();
