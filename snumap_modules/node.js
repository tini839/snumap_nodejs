'use strict'

class Node {
	constructor(idx, coord, neighbors, level) {
		this.idx = idx
		this.coord = coord
		if(neighbors)
			this.neighbors = neighbors
		else
			this.neighbors = []
		this.level = level
	}

	distanceTo(other) {
		let diff = [this.coord[0] - other.coord[0], this.coord[1] - other.coord[1]]
		let rawDist = Math.sqrt(diff[0] * diff[0] + diff[1] * diff[1])

		if(this.level > 1 && other.level > 1)
			return rawDist / 5.0;
		else if(this.level == 1 && other.level == 1)
			return rawDist;
		else
			return rawDist;
	}
}

module.exports = Node