'use strict'

var Node = require('./node')
var NodeTable = require('./nodeTable')

class Navigator {
	constructor(startCoord, endCoord, level, minLevel) {

		this.cloneDic = {};

		this.startCoord = startCoord
		this.endCoord = endCoord
		this.level = level

		this.startNode = this.findNearestNode(this.startCoord, minLevel)
		this.endNode = this.findNearestNode(this.endCoord, minLevel)
		this.route = []
try {
		if(this.startNode.idx == this.endNode.idx) {
			this.route = [this.startNode]
		}
		else {
			this.openNode = new Map()
			this.closeNode = new Map()

			this.startNode.g = 0.0
			this.startNode.h = this.startNode.distanceTo(this.endNode)
			this.startNode.f = this.startNode.g + this.startNode.h
			this.startNode.route = []
			this.openNode.set(this.startNode.idx, this.startNode)

			while(this.openNode.size > 0) {
				let q = null
				let leastF = -1
				for (let node of this.openNode.values()) {
					if(leastF < 0 || leastF > node.f) {
						q = node
						leastF = node.f
					}
				}

				this.openNode.delete(q.idx)

				let rideBus = false;

				if(level > 1 && minLevel < 2 && q.bus) {
					for (let line in q.bus) {
						let neighbor;
						if(q.bus[line].next < 0){
							rideBus = false;
							continue;
						}
						if(!(q.bus[line].next in this.cloneDic)) {
							this.cloneDic[q.bus[line].next] = NodeTable.get(q.bus[line].next);
						}
						neighbor = this.cloneDic[q.bus[line].next]

						if(q.distanceTo(this.endNode) * 1.5 < neighbor.distanceTo(this.endNode)) {
							continue;
						}
						let busnavi = new Navigator(q.coord, neighbor.coord, 2, 2);
						let dist = busnavi.distance;
						let newRoute = q.route.slice(0);
						newRoute = newRoute.concat(busnavi.route.slice(0, busnavi.route.length - 1));

						let newG = q.g + dist
						let newH = neighbor.distanceTo(this.endNode)
						let newF = newG + newH

						if((this.openNode.has(neighbor.idx) || this.closeNode.has(neighbor.idx)) && neighbor.f <= newF)
							continue
						else {
							rideBus = true;
							neighbor.g = newG
							neighbor.h = newH
							neighbor.f = newF
							neighbor.route = newRoute.slice(0)

							if(neighbor.bus) {
								delete neighbor.neighbors[q.bus[line].backward];
							}

							if(!this.openNode.has(neighbor.idx) && !this.closeNode.has(neighbor.idx)) {
								this.openNode.set(neighbor.idx, neighbor)
							}
						}
					}
				}

				if(rideBus == false) {
					for (let key in q.neighbors) {
						let neighbor;
						if(!(q.neighbors[key] in this.cloneDic)) {
							this.cloneDic[q.neighbors[key]] = NodeTable.get(q.neighbors[key]);
						}
						neighbor = this.cloneDic[q.neighbors[key]]
						if(q.level == 2 && neighbor.level == 2 && minLevel < 2)
							continue;
						let dist = q.distanceTo(neighbor)
						let newRoute = q.route.slice(0)
						newRoute.push(q)

						if(neighbor.idx == this.endNode.idx) {
							newRoute.push(this.endNode)
							this.route = newRoute
							this.distance = q.g + q.distanceTo(this.endNode)
							return
						}
						else {
							let newG = q.g + dist
							let newH = neighbor.distanceTo(this.endNode)
							let newF = newG + newH

							if((this.openNode.has(neighbor.idx) || this.closeNode.has(neighbor.idx)) && neighbor.f <= newF)
								continue
							else {
								neighbor.g = newG
								neighbor.h = newH
								neighbor.f = newF
								neighbor.route = newRoute.slice(0)

								if(!this.openNode.has(neighbor.idx) && !this.closeNode.has(neighbor.idx)) {
									this.openNode.set(neighbor.idx, neighbor)
								}
							}
						}
					}
				}
				this.closeNode.set(q.idx, q)
			}
		}
	} catch(err) {console.log(err.stack);}
	}

	findNearestNode(coord, level) {
		let nearest = null
		let minDist = -1
		for(let key in NodeTable.dic) {
			let node = NodeTable.dic[key]
			if(node.level != level)
				continue;
			let diff = [node.coord[0] - coord[0], node.coord[1] - coord[1]]
			let dist = diff[0] * diff[0] + diff[1] * diff[1]
			if(minDist < 0 || dist < minDist) {
				nearest = node
				minDist = dist
			}
		}
		return nearest
	}

	getRoute() {
		let route = {
			startCoord: this.startCoord,
			subroute: this.route,
			endCoord: this.endCoord
		};

		var distance = 0;

		if(route.subroute.length > 1) {
			let head = this.smoothify(route.startCoord, route.subroute[0].coord, route.subroute[1].coord)
			var tmp = route.subroute[0];
			route.subroute[0] = new Node(tmp.idx, head[1], tmp.neighbors, tmp.level);
			let tail = this.smoothify(route.subroute[route.subroute.length - 2].coord, route.subroute[route.subroute.length - 1].coord, route.endCoord)
			var tmp = route.subroute[route.subroute.length - 1];
			route.subroute[route.subroute.length - 1] = new Node(tmp.idx, tail[1], tmp.neighbors, tmp.level);
		}

		for (let key in route.subroute) {
			let val = route.subroute[key];
			let newval = {}
			newval.idx = val.idx;
			newval.coord = val.coord;
			newval.level = val.level;
			if(val.bus)
				newval.bus = val.bus;
			route.subroute[key] = newval;
		}

		var getDistance = function (a, b, bus) {
			var diff = [a[0] - b[0], a[1] - b[1]];
			var dist = Math.sqrt(diff[0] * diff[0] + diff[1] * diff[1]);
			if(bus)
				return dist / 4.0;
			else
				return dist;
		}

		if(route.subroute.length > 0) {
			distance = getDistance(route.startCoord, route.subroute[0].coord, false);
			for (var i = 0; i < route.subroute.length - 1; i++) {
				distance += getDistance(route.subroute[i].coord, route.subroute[i+1].coord, (route.subroute[i].level == 2) && (route.subroute[i+1].level == 2));
			}
			distance += getDistance(route.subroute[route.subroute.length - 1].coord, route.endCoord, false);
		}
		else {
			distance += getDistance(route.startCoord, route.endCoord, false);
		}

		route.distance = distance;

		return route
	}

	smoothify(coord1, coord2, coord3) {
		let inner = function (v1, v2) {
			return v1[0] * v2[0] + v1[1] * v2[1]
		}

		let v21 = [coord1[0] - coord2[0], coord1[1] - coord2[1]]
		let v23 = [coord3[0] - coord2[0], coord3[1] - coord2[1]]
		if (inner(v21, v23) > 0) {
			let k = inner(v21, v23) / inner(v23, v23)
			let v = [coord2[0] + k * v23[0], coord2[1] + k * v23[1]]
			return [coord1, v, coord3]
		}
		else {
			return [coord1, coord2, coord3]
		}
	}

	getDistance() {
		if(this.distance)
			return this.distance
		else
			return 0
	}
}

module.exports = Navigator