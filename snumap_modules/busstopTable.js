'use strict'

var DBConnection = require('./DBConnection');

var BusstopTable = function () {
	this.dic = {};
	this.load();
};

BusstopTable.prototype.load = function () {
	var self = this;
	self.dic = {};
	var query = 'SELECT * FROM busstop;';

	DBConnection.query (query, function (error, rows, fields) {
		if (error)
			throw error;
		else {
			rows.forEach(function (row, idx, arr) {
				self.dic[Number(row.node_idx)] = row.name;
			});
		}
	});
};

module.exports = new BusstopTable();