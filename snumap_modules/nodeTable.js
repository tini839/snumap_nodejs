'use strict'

var Node = require('./node');
var DBConnection = require('./DBConnection');

var NodeTable = function () {
	this.dic = {};
	this.busgraph = {};
	this.load();
};

NodeTable.prototype.load = function () {
	var self = this;
	self.dic = {};
	var query = 'SELECT * FROM node;';

	DBConnection.query (query, function (error, rows, fields) {
		if (error)
			throw error;
		else {
			rows.forEach(function (row, idx, arr) {
				self.dic[Number(row.idx)] = new Node(Number(row.idx), [Number(row.latitude), Number(row.longitude)], [], Number(row.level));
			});
		}

		query = 'SELECT * FROM edge;';
		DBConnection.query (query, function (_error, _rows, _fields) {
			if (_error)
				throw _error;
			else {
				_rows.forEach(function (row, idx, arr) {
					var start = Number(row.start);
					var end = Number(row.end);
					var endNode = self.dic[end];

					if(!self.dic[start].neighbors) {
						self.dic[start].neighbors = [];
					}

					self.dic[start].neighbors.push(end);
				});

				query = 'SELECT * FROM bus;';
				DBConnection.query (query, function (__error, __rows, __fields) {
					if (__error)
						throw __error;
					else {
						__rows.forEach(function (row, idx, arr) {
							var line = row.line;
							var start = Number(row.start);
							var end = Number(row.end);
							var forward = Number(row.forward);
							var backward = Number(row.backward);

							if (!self.dic[start].bus)
								self.dic[start].bus = {};
							self.dic[start].bus[line] = {next: end, forward: forward, backward: backward};
						})
					}
				})
			}
		});
	});
};

NodeTable.prototype.modify = function (data, callback) {
	var nodeQuery = '';
	var edgeQuery = '';

	if (!data) {
		callback();
		return;
	}

	for (var key in data) {
		var node = data[key];
		var idx = Number(node.idx);

		edgeQuery += 'DELETE FROM edge WHERE start = ' + idx + ';\n';
		if(node.exist == 'false') {
			delete this.dic[idx];
			nodeQuery += 'DELETE FROM node WHERE idx = ' + idx + ';\n';
		}
		else {
			var latitude = Number(node.coord[0]);
			var longitude = Number(node.coord[1]);
			var level = Number(node.level);

			var info = new Node (
				idx,
				[latitude, longitude],
				[],
				level
				);

			this.dic[idx] = info;
			nodeQuery += 'INSERT INTO node (idx, latitude, longitude, level)\n';
			nodeQuery += 'VALUES (' + idx + ', ' + latitude + ', ' + longitude + ', ' + level + ')\n';
			nodeQuery += 'ON DUPLICATE KEY\n';
			nodeQuery += 'UPDATE latitude = ' + latitude + ', longitude = ' + longitude + ', level = ' + level + ';\n';

			this.dic[idx].neighbors = [];

			for (var _key in node.neighbors) {
				var neighbor = Number(node.neighbors[_key]);
				this.dic[idx].neighbors.push(neighbor);
				edgeQuery += 'INSERT INTO edge (start, end)\n';
				edgeQuery += 'VALUES (' + idx + ', ' + neighbor + ');\n';
			}
		}
	}

	if (nodeQuery.length > 0) {
		DBConnection.query (nodeQuery, function (error, rows, fields) {
			if (error)
				throw error;
			else if (edgeQuery.length > 0) {
				DBConnection.query (edgeQuery, function (_error, _rows, _fields) {
					if (_error)
						throw _error;
					else
						callback();
				});
			}
			else
				callback();
		});
	}
};

NodeTable.prototype.get = function (idx) {
	var node = this.dic[idx];
	var result = new Node(node.idx, node.coord, node.neighbors, node.level);
	if (node.bus)
		result.bus = node.bus;
	return result;
}

module.exports = new NodeTable();