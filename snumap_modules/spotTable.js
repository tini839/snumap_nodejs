'use strict'

var DBConnection = require('./DBConnection');

var SpotTable = function () {
	this.dic = {};
	this.load();
};

SpotTable.prototype.load = function () {
	var self = this;
	self.dic = {};
	var query = 'SELECT * FROM spot;';

	DBConnection.query (query, function (error, rows, fields) {
		if (error)
			throw error;
		else {
			rows.forEach(function (row, idx, arr) {
				self.dic[Number(row.idx)] = {
					class: row.class,
					latitude: Number(row.latitude),
					longitude: Number(row.longitude),
					name: row.name
				};
			});
		}
	});
};

module.exports = new SpotTable();