var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
// var session = require('client-sessions');
var SessionStore = require('express-mysql-session');
// var crypto = require('crypto');
var nodemailer = require('nodemailer');

//var $ = require('jquery');

var routes = require('./routes/index');
var users = require('./routes/users');
var navi = require('./routes/navi');
var maptool = require('./routes/maptool');
var buildingtool = require('./routes/buildingtool');
var login_modules = require('./routes/login_modules');
var crawling = require('./routes/crawling');
var add_event = require('./routes/add_event');
var get_event = require('./routes/get_event');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.set('env', 'development')

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ extended: true, limit: '50mb' }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(app.root + '/public', {maxAge: 86400000}));

// session
var options = {
	host: 'snumap.com',
	port: 3306,
	user: 'smap',
	password: '1qa2ws3ed!',
	database: 'snumap'
};

var sessionStore = new SessionStore (options);

app.use(session({
	key: 'sid',
	secret: 'secret',
	store: sessionStore,
	resave: true,
	saveUninitalized: true,
	cookieName: 'session',
	duration: 30 * 60 * 1000,
	activeDuration: 5 * 60 * 1000,
}));

app.use('/', routes);
app.use('/users', users);
app.use('/navi', navi);
app.use('/maptool', maptool);
app.use('/buildingtool', buildingtool);
app.use('/login_modules', login_modules);
app.use('/crawling', crawling);
app.use('/add_event', add_event);
app.use('/get_event', get_event);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

module.exports = app;
